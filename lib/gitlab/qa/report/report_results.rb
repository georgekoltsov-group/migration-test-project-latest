# frozen_string_literal: true

module Gitlab
  module QA
    module Report
      # Uses the API to create or update GitLab test cases and issues with the results of tests from RSpec report files.
      class ReportResults < ReportAsIssue
        attr_accessor :testcase_project_reporter, :results_issue_project_reporter, :files, :test_case_project, :results_issue_project, :gitlab

        IGNORE_EXCEPTIONS = ['Net::ReadTimeout'].freeze

        def initialize(token:, input_files:, test_case_project: nil, results_issue_project: nil, dry_run: false, **kwargs)
          @testcase_project_reporter = Gitlab::QA::Report::ResultsInTestCases.new(token: token, input_files: input_files, project: test_case_project, dry_run: dry_run, **kwargs)
          @results_issue_project_reporter = Gitlab::QA::Report::ResultsInIssues.new(token: token, input_files: input_files, project: results_issue_project, dry_run: dry_run, **kwargs)
          @test_case_project = test_case_project
          @results_issue_project = results_issue_project
          @files = Array(input_files)
          @gitlab = testcase_project_reporter.gitlab
        end

        def assert_project!
          return if test_case_project && results_issue_project

          abort "Please provide valid project IDs or paths with the `--results-issue-project` and `--test-case-project` options!"
        end

        def run!
          puts "Reporting test results in `#{files.join(',')}` as test cases in project `#{test_case_project}`"\
               " and issues in project `#{results_issue_project}` via the API at `#{Runtime::Env.gitlab_api_base}`."

          test_results_per_file do |test_results|
            puts "Reporting tests in #{test_results.path}"

            test_results.each do |test|
              puts "Reporting test: #{test.file} | #{test.name}\n"

              report_test(test) if should_report?(test)
            end

            test_results.write
          end
        end

        private

        def report_test(test)
          testcase = testcase_project_reporter.find_or_create_testcase(test)
          # The API returns the test case with an issue URL since it is technically a type of issue.
          # This updates the URL to a valid test case link.
          test.testcase = testcase.web_url.sub('/issues/', '/quality/test_cases/')

          issue, is_new = results_issue_project_reporter.get_related_issue(testcase, test)

          testcase_project_reporter.add_issue_link_to_testcase(testcase, issue, test) if is_new

          testcase_project_reporter.update_testcase(testcase, test)
          results_issue_project_reporter.update_issue(issue, test)
        end

        # Checks if a test result should be reported.
        #
        # @return [Boolean] false if the test was skipped or failed because of a transient error that can be ignored.
        # Otherwise returns true.
        def should_report?(test)
          return false if test.skipped

          if test.report.key?('exceptions')
            reason = ignore_failure_reason(test.report['exceptions'])

            if reason
              puts "Issue update skipped because #{reason}"

              return false
            end
          end

          true
        end

        # Determine any reason to ignore a failure.
        #
        # @param [Array<Hash>] exceptions the exceptions associated with the failure.
        # @return [String] the reason to ignore the exceptions, or `nil` if any exceptions should not be ignored.
        def ignore_failure_reason(exceptions)
          exception_classes = exceptions
            .filter_map { |exception| exception['class'] if IGNORE_EXCEPTIONS.include?(exception['class']) }
            .compact
          return if exception_classes.empty? || exception_classes.size < exceptions.size

          msg = exception_classes.many? ? 'the errors were' : 'the error was'
          "#{msg} #{exception_classes.join(', ')}"
        end
      end
    end
  end
end
